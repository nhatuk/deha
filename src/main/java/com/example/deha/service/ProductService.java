package com.example.deha.service;

import com.example.deha.entity.ProductEntity;

import java.util.List;
import java.util.Optional;

public interface ProductService {
    List<ProductEntity> getAllProduct();
    Optional<ProductEntity> findById(Integer id);
}
