package com.example.deha.service;

import com.example.deha.entity.ProductEntity;
import com.example.deha.repo.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductServiceImpl implements ProductService {
    @Autowired
    ProductRepository productRepository;

    @Override
    public List<ProductEntity> getAllProduct() {
        List<ProductEntity> p = productRepository.findAll();
        return p;
    }

    @Override
    public Optional<ProductEntity> findById(Integer id) {
       return productRepository.findById(id);
    }
}
