package com.example.deha;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DehaApplication {

    public static void main(String[] args) {
        SpringApplication.run(DehaApplication.class, args);
    }

}
