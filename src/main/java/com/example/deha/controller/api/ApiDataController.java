package com.example.deha.controller.api;

import com.example.deha.entity.ProductEntity;
import com.example.deha.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class ApiDataController {
    @Autowired
    ProductService productService;

    @GetMapping("/product")
    public ResponseEntity<List<ProductEntity>> getAllProduct(@ModelAttribute ProductEntity productEntity){
        List<ProductEntity> product = productService.getAllProduct();
        return new ResponseEntity<>(product,HttpStatus.OK);
    }

    @GetMapping("/product/{id}")
    public ResponseEntity<ProductEntity> getOneProduct(@PathVariable("id") Integer id){
        Optional<ProductEntity> product = productService.findById(id);
        System.out.println(product);
        if (product.isPresent()) {
            return new ResponseEntity<>(product.get(), HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}
