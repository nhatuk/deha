$(document).ready(function (){
    function getProductIndex(){
        $.ajax({
            url : "api/product",
            method : "GET",
            dataType : "json",
            success: function (data){
                var tableProduct = $('#tbBody');
                tableProduct.empty();
                $(data).each(function (index, element){
                    tableProduct.append('<tr class="odd gradeX"><td>'+element.id+'</td>' +
                        '<td>'+element.name+'</td>' +
                        '<td>'+element.price+'</td>' +
                        '<td>'+element.qty+'</td>' +
                        '<td>'+element.status+'</td>' +
                        '<td><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalLong"id="#modalLong">Detail</button></td></tr>');
                });
                var msg = "Data have no data";
            },
            error: function (error){
                alert(error);
            }
        });
    }
    getProductIndex();


});